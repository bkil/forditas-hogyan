# Források és további információk

-   [Fordítási útmutató - Szabad szoftverek magyarításához](http://forditas.fsf.hu/html/Utmutato.html): Ezen dokumentum alapja, továbbfejlesztése (terveink szerint) ezen wikiben fog folyni.
-   [PO-fájlok helyesírás-ellenőrzése](http://forditas.fsf.hu/huspell-po.html)
-   [Fordítás HOGYAN](http://tldp.fsf.hu/Forditas-HOGYAN/Forditas-HOGYAN.html): a TLDP útmutatója
-   [Letölthető, többnyelvű Microsoft Glossary](http://www.microsoft.com/globaldev/tools/MILSGlossary.mspx) - utf-8-ra alakítás és a többi nyelv fordításainak törlése (táblázatkezelők kezelik) után egyszerűen grepelhető, hasznos ha valami nagyon ritka kifejezés esetleges megfelelőit keressük. Az átalakításhoz használható parancs: iconv -f utf-16 -t utf-8 msg.csv &gt; u8\_msg.csv
-   [Kereshető, szabadon hozzáférhető, online Microsoft Glossary](http://www.microsoft.com/language/en/us/search.mspx)
-   [Kereshető, nyílt forrású szoftverek fordításait tartalmazó adatbázis](http://open-tran.eu/)
-   [A magyar helyesírás szabályai (Magyar Elektronikus Könyvtár - MEK-01547)](http://mek.oszk.hu/01500/01547/index.phtml)
